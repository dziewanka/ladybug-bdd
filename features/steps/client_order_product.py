from utils import wait_for_overlay_hide

@given(u'Client is on Main Page')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > h1")
    assert e.is_displayed(), "'Czesc' jest niewidoczene"

@when(u'Client click on Order button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(1) > ul > li:nth-child(5) > a")
    e.click()
    assert context.browser.current_url.endswith(
        "#/orders"
    ), "Nie jestes na stronie zamowien"
    e = context.browser.find_element_by_css_selector("div.ng-table-counts > button:last-of-type")
    e.click()
    wait_for_overlay_hide(context)
    list_of_orders = context.browser.find_elements_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr")
    context.number_of_orders = len(list_of_orders)

@when(u'Client click on Add new button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > div > div.action-bar.ng-scope > a")
    e.click()
    assert context.browser.current_url.endswith(
        "#/orders/new"
    ), "Nie jestes na stronie dodawania produktu"


@when(u'Client click on plus button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > form > div.action-bar > button > span")
    e.click()
    f = context.browser.find_element_by_css_selector("body > div:nth-child(3) > form > table > thead > tr.ng-table-sort-header > th:nth-child(3) > div > span")
    assert f.is_displayed(), "'Quantity' jest niewidoczene"


@when(u'Client click on dropdown list Product')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("#category > div.ui-select-match > span > i")
    e.click()
   


@when(u'Client choose Testowy produkt 1')
def step_impl(context): 
    e = context.browser.find_element_by_css_selector("ul.ui-select-choices div.ui-select-choices-row")
    assert e.is_displayed(), "'Produkt 1' jest niewidoczeny"
    e.click()


@when(u'Client fill in the form Quantity with lower than zero value')
def step_impl(context):
    e = context.browser.find_element_by_css_selector('input[ng-model="row.quantity"]')
    e.send_keys('-1')


@when(u'Client click aprove button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("td > button.btn.btn-primary")
    e.click()


@when(u'Client click Save button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("div.button-bar > button.btn-primary")
    e.click()
    f = context.browser.find_element_by_css_selector("div.ng-table-counts > button:last-of-type")
    f.click()

@then(u'The product is not added')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("div.ng-table-counts > button:last-of-type")
    e.click()
    wait_for_overlay_hide(context)
    list_of_orders = context.browser.find_elements_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr")
    assert context.number_of_orders == len(list_of_orders), "Dodano nowy produkt"


@when(u'Client fill in the form Quantity with 1')
def step_impl(context):
    e = context.browser.find_element_by_css_selector('input[ng-model="row.quantity"]')
    e.send_keys('1')


@then(u'The product are added')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("div.ng-table-counts > button:last-of-type")
    e.click()
    wait_for_overlay_hide(context)
    list_of_orders = context.browser.find_elements_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr")
    assert context.number_of_orders < len(list_of_orders), "Nie dodano nowego produktu"


@then(u'The order has got Draft Status')
def step_impl(context):
    list_of_orders = context.browser.find_elements_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr")
    last_order = list_of_orders[-1]
    last_order_status = last_order.find_element_by_css_selector('td[data-title-text="Status"]')
    assert last_order_status.text == "Robocze", "Produkt nie ma statusy Robocze"


@when(u'Client click on Cancel button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("div.ng-table-counts > button:last-of-type")
    e.click()
    wait_for_overlay_hide(context)
    f = context.browser.find_element_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td.ng-scope > a:nth-child(2)")
    f.click()
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(6)")
    context.wartosc_of_orders = e.text
    list_of_orders = context.browser.find_elements_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr")
    first_order = list_of_orders[0]
    first_order_status = first_order.find_element_by_css_selector('td[data-title-text="Status"]')
    assert first_order_status.text == "Anulowane", "Produkt nie ma statusu Anulowane"
    

@when(u'Client click on Edit button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td.ng-scope > a:nth-child(1)")
    e.click()


@when(u'Client click on Edit_1 button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > form > table > tbody > tr > td:nth-child(4) > button.btn.btn-default.btn-sm.ng-scope")
    e.click()


@when(u'Client fill in the form Quantity with different amount than given')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > form > table > tbody > tr > td:nth-child(3) > div > input")
    e.clear()
    e.send_keys(str(int(context.wartosc_of_orders)+10))


@then(u'Quantity in PLN is the same as ealier')
def step_impl(context):
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td:nth-child(6)")
    context.new_wartosc_of_orders = e.text
    assert context.wartosc_of_orders == context.new_wartosc_of_orders , 'Zamowienie zostalo edytowane'



from steps.utils import wait_for_overlay_hide, on_login_page
from steps.utils import login_with_username_with_password

@given("user is on the login page")
def user_is_on_logowanie_page(context):
    context.browser.get(context.base_url)
    e = context.browser.find_element_by_css_selector("body > div:nth-child(3) > h1")
    assert e.is_displayed(), "'Czesc' jest niewidoczene"
    e = context.browser.find_element_by_css_selector(
        "body > div:nth-child(1) > ul > li:nth-child(2) > a"
    )
    assert e.is_displayed(), "'Logowanie' link jest niewidoczne"
    e.click()
    assert context.browser.current_url.endswith(
        "#/login"
    ), "Nie jestes na stronie logowania"


@then("user can see log out tab")
def user_can_see_wylogownie_tab(context):
    e = context.browser.find_elements_by_css_selector('a[ng-click="logout()"]')
    assert any(e), "Cannot see wylogowanie tab"


@then("user can see login greeting")
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "body > div:nth-child(3) > div:nth-child(2) > p:nth-child(3)"
    )
    actual_text = e.text
    expected_text = u"Przejdź do zakładki Zamówienia, aby podejrzeć swoje zamówienia lub dodać nowe."
    assert actual_text == expected_text, "Greting was diffrent"


@when("user fills in the sign in form and submits it")
def user_fills_in_the_sign_in_form_and_submits_it(context):
    assert on_login_page(context), "Cannot see login page"
    login_with_username_with_password(
        context, context.test_config["username"], context.test_config["password"]
    )

@when(
    'user fills in the Sing in form with login "{username}" and password "{password}" and submits it'
)
def step_impl(context, username, password):
    assert on_login_page(context), "Cannot see login page"
    login_with_username_with_password(context, username, password)


@when(
    'user fills in the Sing in form with login "{username}" and empty password and submits it'
)
def step_impl(context, username):
    assert on_login_page(context), "Cannot see login page"
    login_with_username_with_password(context, username, "")


@when("user fills in the sign in form")
def step_impl(context):
    assert on_login_page(context), "Cannot see login page"
    context.browser.find_element_by_css_selector("div.overlay-loader.ng-hide")


@when('with login "{username}"')
def step_impl(context, username):
    context.username = username


@when("with empty login")
def step_impl(context):
    context.username = ""


@when('with password "{password}"')
def step_impl(context, password):
    context.password = password


@when("with empty password")
def step_impl(context):
    context.password = ""


@when("submits sign in form")
def step_impl(context):
    login_with_username_with_password(context, context.username, context.password)


@then(u"user can not see log out tab")
def step_impl(context):
    elements = context.browser.find_elements_by_css_selector('a[ng-click="logout()"]')
    assert not any(map(lambda e: e.is_displayed(), elements)), "Can see Wylogowanie tab"
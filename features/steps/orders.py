from utils import wait_for_overlay_hide

@given("user is on Order page")
def step_impl(context):
    wait_for_overlay_hide(context)
    e = context.browser.find_element_by_css_selector('a[href="#/orders"]')
    assert e.is_displayed(), "Orders link jest niewidoczne"
    e.click()
    wait_for_overlay_hide(context)
    assert context.browser.current_url.endswith(
        "#/orders"
    ), "Nie jestes na stronie Order"


@when(u'add the product on the basket')
def step_impl(context):
    raise NotImplementedError(u'STEP: When add the product on the basket')


from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def check_if_overlay_hiden(browser):
    return browser.find_element_by_css_selector("div.overlay-loader.ng-hide")

def wait_for_overlay_hide(context):
    WebDriverWait(context.browser, 10).until(check_if_overlay_hiden)

def on_login_page(context):
    any_login_input = context.browser.find_elements_by_css_selector('#username')
    return any(any_login_input)

def login_with_username_with_password(context, username, password):
    e = context.browser.find_element_by_css_selector('#username')
    e.clear()
    e.send_keys(username)
    e = context.browser.find_element_by_css_selector('#password')
    e.clear()
    e.send_keys(password)
    e = context.browser.find_element_by_css_selector('body > div:nth-child(3) > form > button')
    e.click()

def wait_for_user_is_logged(context):
    return WebDriverWait(context.browser, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, 'a[ng-click="logout()"]')),
        "Cannot click log out link - user isn't logged"
    )

def set_trace_with_pdb():
    import pdb
    import sys

    for attr in ("stdin", "stdout", "stderr"):
        setattr(sys, attr, getattr(sys, "__%s__" % attr))
    pdb.set_trace()


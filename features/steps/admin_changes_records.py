from steps.utils import wait_for_overlay_hide

first_name = "Anna"
last_name = "Nowak"
Street = "Mickiewicza"
home_no = "10"
flat_no = "20"
City = "Sopot"
zip_code = "80-100"

@given(u'User is on Clients page')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        'a[href="#/clients"]'
        )
    e.click()
    wait_for_overlay_hide(context)
    assert context.browser.current_url.endswith(
        "#/clients"
    ), "Nie jestes na stronie Klienci"


@when(u'User clicks on Clients Edit button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        ".table > tbody > tr:nth-of-type(1) > td:nth-of-type(8) > a:nth-child(1)"
    )
    e.click()


@when(u'user fills in the Clients form')
def step_impl(context):
    firstName = context.browser.find_element_by_css_selector(
        "#firstName"
    )
    firstName.clear()
    firstName.send_keys(first_name)

    lastName = context.browser.find_element_by_css_selector(
        "#lastName"
 )
    lastName.clear()
    lastName.send_keys(last_name)

    street = context.browser.find_element_by_css_selector(
        "#street"
    )
    street.clear()
    street.send_keys(Street)

    homeNo = context.browser.find_element_by_css_selector(
        "#homeNo"
    )
    homeNo.clear()
    homeNo.send_keys(home_no)

    flatNo = context.browser.find_element_by_css_selector(
        "#flatNo"
    )
    flatNo.clear()
    flatNo.send_keys(flat_no)

    city = context.browser.find_element_by_css_selector(
        "#city"
    )
    city.clear()
    city.send_keys(City)

    zipCode = context.browser.find_element_by_css_selector(
        "#zipCode"
    )
    zipCode.clear()
    zipCode.send_keys(zip_code)

@when(u'user clicks on Client Save button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "div.button-bar > button.btn.btn-primary.ng-scope"
    )
    e.click()
    edited = context.browser.find_element_by_css_selector("tbody > tr:nth-of-type(1)")
    context.content_of_table = edited.text


@then(u'the edited Client is saved')
def step_impl(context):
    saved = context.browser.find_element_by_css_selector("tbody > tr:nth-of-type(1)")
    context.new_content_of_table = saved.text
    assert context.content_of_table == context.new_content_of_table , 'Klient nie zostal edytowany'


@given(u'User is on Products page')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "a[href='#/products']"
        )
    e.click()
    wait_for_overlay_hide(context)
    assert context.browser.current_url.endswith(
        "#/products"
    ), "Nie jestes na stronie Produkty"


@when(u'User clicks on Products Edit button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "tbody > tr:nth-of-type(1) > td:nth-of-type(8) > a:nth-child(1)"
        )
    e.click()
    wait_for_overlay_hide(context)

Name = "Testowy produkt xxx"
Company = "IKEA"
Description = "Lorem ipsum"
Price = "999999"
expiration_date = "2020-03-04"

@when(u'user fills in the Product form')
def step_impl(context):
    name = context.browser.find_element_by_css_selector(
        "#name"
    )
    name.clear()
    name.send_keys(Name)

    
    category_cb = context.browser.find_element_by_css_selector(
        "div.ui-select-container"
    )
    category_cb.click()

    category_item = context.browser.find_element_by_css_selector(
        ".ui-select-choices-row"
    )
    category_item.click()

    company = context.browser.find_element_by_css_selector(
        "#company"
    )
    company.clear()
    company.send_keys(Company)

    description = context.browser.find_element_by_css_selector(
        "#description"
    )
    description.clear()
    description.send_keys(Description)

    price = context.browser.find_element_by_css_selector(
        "#price"
    )
    price.clear()
    price.send_keys(Price)

    expirationDate = context.browser.find_element_by_css_selector(
        "#expirationDate"
    )
    expirationDate.clear()
    expirationDate.send_keys(expiration_date)


@when(u'user clicks on Save Product button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "div.button-bar > button.btn.btn-primary.ng-scope"
    )
    e.click()


@then(u'the edited product is saved')
def step_impl(context):

    wait_for_overlay_hide(context)
   
    assert context.browser.current_url.endswith(
        "#/products"
    ), "Jestes na stronie produkty"
  
@when(u'User clicks on Products Remove button')
def step_impl(context):
    e = context.browser.find_element_by_css_selector(
        "body > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td.ng-scope > a:nth-child(2)"
    )
    wait_for_overlay_hide(context)
    e.click()

@then(u'the selected product is removed')
def step_impl(context):
    alert_content = context.browser.find_element_by_css_selector(
        ".alert"
    )
    wait_for_overlay_hide(context)
    assert  context.browser.current_url.endswith(
        "#/clients"
    ), "Nie jestes na stronie klienci"

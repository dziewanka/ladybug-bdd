@manual
Feature: sign in to employee account
    As a employee I want to log in

    Scenario: Log in with valid login and password
        Given user is on the login page
        When user fills in the Sign In form and submits it
        Then user can see Wylogownie tab

    Scenario: Login in with valid login and invalid password
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

    Scenario: Login in with invalid login and valid password
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab
    
    Scenario: Login in with invalid login and password
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

    Scenario: Login in with empty login and valid password
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

    Scenario: Login in with valid login and epmty password
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

    Scenario: Login in with empty login and invalid password    
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

    Scenario: Login in with invalid login and empty password    
        Given user is on the login page
        When user fills in the Sing in form and submits it     
        Then user can not see log out tab

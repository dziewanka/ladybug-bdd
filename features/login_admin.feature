@fixture.admin
Feature: sign in to admin account
    As an admin I want to log in
    
    
    @fixture.clear_cookies
    Scenario: Log in with valid login and password
        Given user is on the login page
        When user fills in the sign in form and submits it
        Then user can see log out tab
        And user can see login greeting

    @fixture.clear_cookies
    Scenario: Login in with valid login and invalid password
        Given user is on the login page
        When user fills in the sign in form
        And with login "admin" 
        And with password "invalid" 
        And submits sign in form    
        Then user can not see log out tab

    @fixture.clear_cookies  
    Scenario: Login in with invalid login and valid password
        Given user is on the login page
        When user fills in the sign in form
        And with login "invalid" 
        And with password "test" 
        And submits sign in form     
        Then user can not see log out tab
    
    @fixture.clear_cookies
    Scenario: Login in with invalid login and invalid password
        Given user is on the login page
        When user fills in the sign in form
        And with login "invalid" 
        And with password "invalid" 
        And submits sign in form 
        Then user can not see log out tab
    
    @fixture.clear_cookies
    Scenario: Login in with empty login and valid password
        Given user is on the login page    
        When user fills in the sign in form
        And with empty login 
        And with password "test" 
        And submits sign in form 
        Then user can not see log out tab

    @risk-high
    @bug
    @LB-001
    @fixture.clear_cookies
    Scenario: Login in with valid login and empty password
        Given user is on the login page
        When user fills in the sign in form 
        And with login "admin" 
        And with empty password 
        And submits sign in form   
        Then user can not see log out tab

    @fixture.clear_cookies
    Scenario: Login in with empty login and invalid password    
        Given user is on the login page
        When user fills in the sign in form
        And with empty login 
        And with password "invalid" 
        And submits sign in form     
        Then user can not see log out tab

    @fixture.clear_cookies
    Scenario: Login in with invalid login and empty password    
        Given user is on the login page
        When user fills in the sign in form
        And with login "invalid"
        And with empty password 
        And submits sign in form    
        Then user can not see log out tab

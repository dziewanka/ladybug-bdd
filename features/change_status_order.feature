@manual
Feature: chanage status order by employee
    As a employee I want change a status order
   
    Scenario: change status order from "working" to "accepted"
        Given status order is working  
        When change a status to accepted 
        Then status order is changed to accepted

    Scenario: change status order from "accepted" to "sent to packaging"
        Given status order is accepted  
        When change a status to sent to packing 
        Then status order is changed to sent to packing

    Scenario: change status order from "sent to packing" to "delivery"
        Given status order is sent to packing  
        When change a status to delivery 
        Then status order is changed to delivery 

    Scenario: change status order from "delivery" to "done"
        Given status order is delivery  
        When change a status to done 
        Then status order is changed to done 

    Scenario: change status order from "working" to "cancel"
        Given status order is working  
        When change a status  to cancel 
        Then status order is changed to cancel  

    Scenario: change status order from "accepted" to "cancel"
        Given status order is accepted  
        When change a status to cancel
        Then status order is changed to canceled  

    Scenario: change status order from "sent to packing" to "cancel"
        Given status order is sent to packaging  
        When change a status to cancel 
        Then status order is changed to cancel

    Scenario: change status order from "delivery" to "cancel"
        Given status order is delivery
        When chanage a status to cancel
        Then status order is change to cancel

    Scenario: change status order from "done" to "cancel"
        Given status order is done 
        When user change a status order to cancel   
        Then status order is not change

    Scenario: change status order from "done" to "delivery"
        Given status order is done   
        When user change a status order to delivery 
        Then status order is not change 

    Scenario: change status order from "delivery" to "sent to packing"
        Given status order is delivery
        When user change a status order to sent to packing 
        Then status order is not change

    Scenario: change status order from "sent to packing" to "accept"
        Given status order is sent to packing 
        When user change a status order to accept
        Then status order is not change

    Scenario: change status order from "accept" to "working"
        Given status order is accept
        When user change a status order to working
        Then status order is not change
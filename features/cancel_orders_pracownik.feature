@fixture.logged_as_employee
Feature: Cancel orders as employee
    As an employee I want to cancel an order
    
    Scenario: Employee cancels the Order on Orders page
        Given User is on Orders page
        When User clicks on Cancel Order button
        Then the Order is cancelled   
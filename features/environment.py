import subprocess
import time
import urllib.request
from behave import fixture, use_fixture
from behave.fixture import use_composite_fixture_with, fixture_call_params
from selenium import webdriver
from steps.utils import wait_for_overlay_hide, on_login_page, login_with_username_with_password
from steps.utils import wait_for_user_is_logged
from steps.utils import set_trace_with_pdb


@fixture
def browser_chrome(context):
    context.browser = webdriver.Chrome()
    context.browser.implicitly_wait(3)
    yield context.browser
    context.browser.quit()


@fixture
def base_url(context):
    context.base_url = "http://localhost:8080"


@fixture
def config_admin(context):
    context.test_config = {"username": "admin", "password": "test"}


@fixture
def clear_cookies(context):
    context.browser.delete_all_cookies()


@fixture
def logged_as_any_user(context, username, password):
    context.browser.get(context.base_url + "/#/login")
    context.browser.refresh()
    wait_for_overlay_hide(context)
    assert on_login_page(context), "Cannot see login page"
    login_with_username_with_password(
        context, username, password
    )
    assert wait_for_user_is_logged(context), "User isn't loged in"
    yield
    try:
        wait_for_overlay_hide(context)
    except:
        pass
    e = context.browser.find_element_by_css_selector('a[ng-click="logout()"]')
    e.click()

@fixture
def logged_as_admin(context):
    as_user_composite = use_composite_fixture_with(context, [
        fixture_call_params(logged_as_any_user, username="admin", password="test")
    ])
    return as_user_composite


@fixture
def logged_as_client(context):
    as_user_composite = use_composite_fixture_with(context, [
        fixture_call_params(logged_as_any_user, username="client", password="test")
    ])
    return as_user_composite


@fixture
def logged_as_employee(context):
    as_user_composite = use_composite_fixture_with(context, [
        fixture_call_params(logged_as_any_user, username="employee", password="test")
    ])
    return as_user_composite
    

def goto_main_page(context):
    context.browser.get(context.base_url)


BEHAVE_DEBUG_ON_ERROR = False


def setup_debug_on_error(userdata):
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool("BEHAVE_DEBUG_ON_ERROR")


def after_step(context, step):
    if BEHAVE_DEBUG_ON_ERROR and step.status == "failed":
        import pdb
        import sys

        for attr in ("stdin", "stdout", "stderr"):
            setattr(sys, attr, getattr(sys, "__%s__" % attr))
        pdb.post_mortem(step.exc_traceback)


def before_all(context):
    use_fixture(base_url, context)
    use_fixture(browser_chrome, context)
    setup_debug_on_error(context.config.userdata)


def before_tag(context, tag):
    if tag == "fixture.admin":
        use_fixture(config_admin, context)
    if tag == "fixture.clear_cookies":
        use_fixture(clear_cookies, context)
    if tag == "fixture.logged_as_admin":
        use_fixture(logged_as_admin, context)
    if tag == "fixture.logged_as_client":
        use_fixture(logged_as_client, context)
    if tag == "fixture.logged_as_employee":
        use_fixture(logged_as_employee, context) 
    if tag == "fixture.goto_main_page":
        use_fixture(goto_main_page, context)

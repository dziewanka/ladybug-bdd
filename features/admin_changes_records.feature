
@fixture.logged_as_admin
Feature: Changing records by admin
    As an admin I want to change the records 
    
    @risk-low
    @bug
    @LB-005
    Scenario: Admin changes records on Clients page
        Given User is on Clients page
        When User clicks on Clients Edit button
        And user fills in the Clients form
        And user clicks on Client Save button
        Then the edited Client is saved
    
  
    Scenario: Admin changes records on Products page
        Given User is on Products page
        When User clicks on Products Edit button
        And user fills in the Product form
        And user clicks on Save Product button
        Then the edited product is saved

    @risk-high
    @bug
    @LB-006
    Scenario: Admin removes the Product on Products page
        Given User is on Products page
        When User clicks on Products Remove button 
        Then the selected product is removed
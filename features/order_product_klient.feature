@fixture.logged_as_client
Feature: order product by the client


@risk-medium
@bug
@LB-002
@fixture.goto_main_page
Scenario: order negativie amount of products
  Given Client is on Main Page
   When Client click on Order button
    And Client click on Add new button
    And Client click on plus button
    And Client click on dropdown list Product
    And Client choose Testowy produkt 1
    And Client fill in the form Quantity with lower than zero value
    And Client click aprove button
    And Client click Save button
   Then The product is not added


@fixture.goto_main_page
Scenario: order positive amount of products
  Given Client is on Main Page
   When Client click on Order button
    And Client click on Add new button
    And Client click on plus button
    And Client click on dropdown list Product
    And Client choose Testowy produkt 1
    And Client fill in the form Quantity with 1
    And Client click aprove button
    And Client click Save button
   Then The product are added
    And The order has got Draft Status

@LB-003
@bug
@fixture.goto_main_page
Scenario: Edit the order in Status Canceled
  Given Client is on Main Page
   When Client click on Order button
    And Client click on Cancel button
    And Client click on Edit button
    And Client click on Edit_1 button
    And Client fill in the form Quantity with different amount than given
    And Client click aprove button
    And Client click Save button
   Then Quantity in PLN is the same as ealier






   
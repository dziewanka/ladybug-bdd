# Instalacja i uruchamianie Ladybug

W projekcie wykorzystano aplikację Ladybug, która jest przykładem sklepu on-line.

Apliakcja powstała na wydarzenie testerskie Hackathon Testerski organizowany przez grupę Girsl Who Test. Dostępna pod adresem: http://javagirl.pl/aplikacje/ladybug-e-zakupy/.

Do uruchomienia aplikacji wymagana jest Java JDK. Można ją pobrac tutaj: https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html. 

Aplikację uruchamia się z konsoli wpisując:

```sh
java -jar ladybug-0.0.1-SNAPSHOT.jar
```

Uruchomiona aplikacja dostępna jest pod domyślnym adresem http://localhost:8080 

Zamknięcie aplikacji odbywa się poprzez użycie kombinacji klawiszy `CTRL+C`. 

# Przygotowanie środowiska testowego

Przygotowaując środowisko testowe należy dodać drugą konsolę.

Wymaganiem uruchomienia testów jest zainstalowany interpretator języka Python w wersji co najmniej 3.7. Instalująć Python 3.7 należy pamiętać aby zaznaczyć opcję `Add to Path`.

Do odizolowania środowiska testowego od domyślnej instalacji Pythona wykorzystano narzędzie `venv`.

```sh
python -m venv venv
```

Aktywujemy to środowisko komendą 

```sh
source venv/Scripts/activate
```

Następnie instalujemy zależności projektu. 

```sh
python -m pip install -r requirements.txt
```

Notatka:
 
Plik dotyczący zależności projektu requirements.txt został stworzony, poprzez wykonanie komend

```sh
pip install behave
pip install selenium
```

Wskazówki jak to zrobić są w tutorialu: https://testuj.pl/blog/test-automatyczny-bdd-w-selenium-z-wykorzystaniem-python-i-behave-instrukcja-krok-po-kroku/.

Należy pobrać sterownik przeglądarki Chrome - chromedriver ze strony https://chromedriver.storage.googleapis.com/index.html?path=74.0.3729.6/ i zainstalować go w głównym katalogu aplikacji.

W konsoli wpisujemy komendę:

```sh
mv ~/Downloads/chromedriver_win32.zip .
```
Co spowoduje, że chromedriver zostanie przesunięty do katalogu projektu.

```sh
unzip chromedriver_win32.zip
```
Co umożliwi rozpakowanie chromedriver.

```sh
 ls
```
Dzięki czemu sprawdzimy wykonane zmiany.

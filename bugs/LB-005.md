## Clients data zip code can not be provided with dash.

**Severity:** Low

**Priority:** High

Description: This need to be fixed before application is given to client.

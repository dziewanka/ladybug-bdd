# Ladybug BDD

Ladybug BDD jest projektem wykonującym automatyczne testy funkcjonalne według filozofii Behavior Driven Development.

W projekcie wykorzystujemy język Python wraz z bibliotekami Behave i Selenium.

Testowaną przez nas aplikacją jest Ladybug. 

Projekt został utworzony na potrzeby pracy dyplomowej kierunku Tester oprogramowania 2018/19 w Wyższej Szkole Bankowej w Gdańsku. 

## Ladybug

Z dostępnego opisu aplikacji:

> Aplikacja ma celu realizację zakupów online, gdzie występują trzy role: administrator, pracownik oraz klient. Administrator i pracownik są na stałe i nie zatrudniamy nikogo więcej. Natomiast klienci mogą się rejestrować tyle razy ile chcą.

Użytkownicy Ladybug to:
- administrator,
- pracownik,
- klient,
- niezarejestrowany użytkownik. 

Główne funkcjonalności testowanej aplikacji to:
1. logowanie
2. proces zamówienia produktu przez klienta
3. zmiana statusu zamówienia przez pracownika
4. edycja rekordów związanych z aplikacją przez admina

Opis funkcjonalności znajdziemy w dokumentacji:

https://docs.google.com/document/d/1eVr8l5XwP0gygC5D7OnWKFVIsndU4_jxz67uQKb_n2g/edit?usp=sharing


## Testowanie

Szczegółowy opis funkcjonalności został stworzony w plikach testowych behave w katalogu `features` i podkatalogach. 

### Instalacja testowanej aplikacji i środowiska testowego

Opis instalacji znajdziemy w pliku INSTALL.md .

### Uruchomienie testów automatycznych

Aktywuj środowisko testowe (dokładny opis instalacji środowiska testowego znajduje się w pliku INSTALL.md):

```sh
source venv/Scripts/activate
```

Uruchom testy automatyczne : 

```sh
behave
```

Uruchom testy automatyczne z wybranym tagiem np. @focus:

```sh
behave --tags=@focus -k 
```


Inne tagi użyte podczas wykonywania testów:

- @bug
- @LB-00X
- @risk-high
- @manual

Standardowe uruchomienie jest zapisane w pliku `behave.sh`, uruchamiamy wtedy Ladybug i wszystkie testy oprócz **@manual**. 

```
./behave.sh 
```

Aby uruchomić testy w trybie debugowania należy użyć flagi **BEHAVE_DEBUG_ON_ERROR**, np.:

```
behave -D BEHAVE_DEBUG_ON_ERROR
```

To spowoduje, że w przypadku wystąpienia błędy uruchomiony zostanie debbuger **pdb**.
Jest to opisane w https://behave.readthedocs.io/en/latest/tutorial.html?highlight=DEBUG#debug-on-error-in-case-of-step-failures.

### Tworzenie nowych przypadków testowych


Każda z testowanych funkcjonalności w BDD jest nowym plikiem z rozszerzeniem `.feature`. 
W tymże pliku tworzymy przykłady użycia scenariuszy testowych `Scenario` a w nim zaimplementowane w postaci klas:

- `Given` początkowy kontekst
- `When` opis wykonywanego zdarzenia
- `Then` sprawdzenie oczekiwanego rezultatu.

Gdy scenariusze są już przygotowane należy je zaimplementować w Python, które tworzymy w katalogu `steps` z rozszerzeniem `.py`.

W projekcie Ladybug BDD są testowane różne funkcjonalności dlatego każda z nich ma swój oddzielny plik w katalogu `steps`. 

### @fixture    

Tworząc testy w projekcie Ladybug - BDD zostały użyte `@fixture`, które wspomagają testowanie. Dzięki nim możemy ustawiać naszą aplikację względem potrzeb, tworzyć lub usuwać obiekty w bazie danych aplikacji jak również automatyzować przygotowanie do testu  i sprzątanie po nim. 

@fixture zostały umieszczone w pliku `environmnet.py` m.in.:  
- clear_cookies
- browser_chrome
- base_url
- logged_as_any_user

### Zatwierdzanie zmian w projekcie w menadżerze repozytoriów (GitLab) 
 
Szczegółowy opis działań dotyczących użytkowania menadżera repozytoriów znajdziemy tutaj: https://docs.gitlab.com/ee/user/project/merge_requests/            

Poniżej skrót działań jakie są wymagane w ramch projektu Ladybug-BDD: 

Po wprowadzeniu jakichkolwiek zmian w repozytorium, należy utworzyć nowy **branch** na którego należy zapisać obiekt zmian **commit**. Wszystkie zmiany jakie chcemy uaktualnić do gałęzi **master** należy uaktualnić za pomocą opcji **Push**. 
Potem należy utworzyć **merge request**  w Gitlabie.    
Po dodaniu **merge request**  powinien dostać  **approve**  od innego użytkownika. **Merge request** który ma status **approve** powinien zostać zmergowany do głównego brancha **master**.
Po zmergowaniu należy zmienić  branch na główny **master** i wykonać **pull** w celu uaktualnienia repozytorium.


### Zakres testów automatycznych

Przeprowadzono testy automatycznych w zakresie:

1. Logowania przez administratora.
    1.1. Logowanie administratora przy użyciu prawidłowego loginu i prawidłowego hasła.
    1.2. Logowanie administratora przy użyciu prawidłowego loginu i nieprawidłowego hasła.
    1.3. Logowanie administratora przy użyciu nieprawidłowego loginu i prawidłowego hasła.
    1.4. Logowanie administratora przy użyciu nieprawidłowego loginu i nieprawidłowego hasła.
    1.5. Logowanie administratora bez użycia loginu i z użyciem prawidłowego hasła.
    1.6. Logowanie administratora przy użyciu prawidłowego loginu i bez użycia hasła.
    1.7. Logowanie administratora bez użycia loginu i z użyciem nieprawidłowego hasła. 
    1.8. Logowanie administratora przy użyciu nieprawidłowego loginu i bez użycia hasła. 
2. Składania i edycji zamówień przez klienta:
    2.1. Zamówienie przez klienta ujemnej liczby produktów.
    2.2. Zamówienie przez klienta dodatniej liczby produktów.
    2.3. Edycja zamówienia w statusie `Anulowane` przez klienta.
3. Edycja rekorów zwiazancyh z aplikacją przez administratora
    3.1. Zmiana rekordów na stronie klienci
    3.2. Zmiana rekordów na stronie produkty
    3.3. Usunięcie produktu.
    
Szczegółowe informacje o przypadkach testowych można znaleźć w folderze `features`.

## Raporty

### Raporty automatyczne

```


3 features passed, 0 failed, 3 skipped
9 scenarios passed, 0 failed, 34 skipped
56 steps passed, 0 failed, 120 skipped, 0 undefined
Took 0m5.204s

```
Powyżej przedstawiono wynik uruchomienia testów automatycznych za pomocą `behave`
Można zauważyć, że w powyższym przykładzie zakończono sukcesem : 3 feature, 9 scenariuszy testowych i 56 stepów.

Można też zauważyć w jakim czasie wykonują się testy automatyczne. 

###  Znalezione błędy

Znaleziono następujące błędy w aplikacji:
1. `LB-001` - administrator jest w stanie zalogować się bez podawania hasła. -> przypadek testowy `1.6`.
2. `LB-002` - klient jest w stanie zamówić ujemną liczbę produktów. -> przypadek testowy `2.1`.
3. `LB-003` - klient jest w stanie edytować zamówienie przy statusie `Anulowane`. -> przypadek testowy `2.3`.
4. `LB-004` - klient może zalogowacć się raz na trzy próby logowania ->przypadek testowy `2.1`, `2.2` i `2.3`
5. `LB-005` - administrator nie można utworzyć zmienić formularza klienta  -> przypadek testowy `3.1`
6. `LB-006` - administrator nie może usunąć produktu -> przypadek testowy `3.3`

Szczegółowe informacje o błędach można znaleźć w folderze `bugs`.

## Wnioski

Aplikacja Ladybug została stworzona dla początkujących testerów w celu doskonalenia umiejętności znajdowania i raportowania błędów. Pod względem szkoleniowo - edukacyjnym aplikacja w pełni spełnia swoją funkcję, stanowi doskonałą bazę do przeprowadzania testów eksploracyjnych, jak również automatycznych, jak w przypadku naszego projektu.

Pod wzglądem biznesowym aplikacja nie spełnia podstawowych wymagań postawionych w specyfikacji, co uniemożliwia udostępnienie jej klientom jako platformy zakupowej.

## Problemy


#!/bin/bash 

echo "Start ladybug"
# java -jar ladybug-0.0.1-SNAPSHOT.jar > ladybug.log 2>&1 &
# LADYBUG_PID=$!

echo "Wait for ladybug starts"
# sleep 60

echo "Run tests"
behave --tags=~@manual --tags=~@bug -k $@

echo "Stop ladybug"
# kill $LADYBUG_PID
# sleep 5

echo "Kill ladybug"
# kill -9 $LADYBUG_PID